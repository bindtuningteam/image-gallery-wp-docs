![generaloptions](../images/modern/15.filtersettings.png)
___

### Filters

Here you define which columns of your document library will be used as filtering options. Each selected filter will be displayed as a dropdown element, with filter values.

The following example shows 2 filters: Extension and Author (therefore 2 mapped columns):

|**Configuration**|**Result**|
|-|-|
|![dropdowns_filters.png](../images/classic/30.dropdowns_internal.png)|![dropdowns_filters.png](../images/classic/29.dropdowns_filters.png)|

If no filters are defined, no dropdowns/filters will appear on the web part.<br />
To use a column as a filter, you need to place it's **internal name** into the field.<br />
To show a custom **display name** on the dropdown, place your text inside square brackets<br />
To have a **default value** selected, add two colons after the interal or display name followed by the selected value.

**e.g.: Dprt[Department]::HR, Topic::Web**


Here is what you need to do on SharePoint to check the column's internal name:

1. Access the **Teams Settings**

    ![settings_edit.png](../images/msteams/01.edit.png)

2. Click on the icon to open the **List**. 

    ![visit_list](../images/msteams/04.visit_list.png)

3. On the top menu, click on **Library Settings**

    ![visit_list](../images/msteams/05.library_settings.png)

4. On the Columns section, click to open the colum name you want to use

5. Inside, on the URL look for "...Field=...".

6. Copy the internal name

![internal-column-name](../images/classic/25.internalcolumnname.png)

____
### Open Filters On

Here you are able to choose how to open the dropdown, on **Click** or on **Hover**.

___
### Search Bar

If enabled, a search bar is added to the top of the web part for filtering content. 
The search bar will always be enabled if any filters are set.