
### Images Source ###

Here you will need to connect your web part with a Documents Library. Without it, the web part will not function and an notification message will appear. Let's get to it:

 ![list-picker](../images/msteams/02.listpicker.gif)

1. On **Images Source**, click **Add List**

2. On the first text box insert one of the two:

 - **The URL of the Documents Library**
 - **A URL of a site collection**

<p class="alert alert-warning">
Use relative paths for this field. So instead of using an URL like <strong>"https://company.sharepoint.com/sites/Home”</strong>, you should use something like <strong>“/sites/Home/”</strong>.
</p>

3. On the second box, select the list if not already selected;

4. On Filtering Options section, select how you want to filter your items:

 - No Filters - All the items from your list will be retrieved and displayed on the web part.

 - CAML Query - This advanced option gives you full control over how you filter your results. Selecting this option, a text field appears to write your own CAML Query. We recommend the use of the free to use tool [U2U Caml Query Builder](https://www.u2u.be/software/), to help write CAML Queries.

 - Other Options - Under the first 2 options, your list views will be listed.

 - On the Field Mappings section, map the fields from the list with the fields of the web part

You will find dropdowns for each of the web part's fields. These dropdowns include all the columns in your list's selected view. Select which column corresponds to which field or select No Mapping when nothing fits the bill. If you want to repeat the same column for different fields that's ok too.

5. When you're done, lock your selection by clicking the save icon.

<p class="alert alert-info">
The supported file types are: <strong>jpg, jpeg, gif, bmp, png</strong> for images and <strong> mp4 </strong> for videos.
</p>


