![configurepanel](../images/msteams/02.configurepanel.png)

On the Web Part Properties panel, you've multiple options which you can edit for diferent configuration of the Web Part.

- [Images Source](./listsettings.md)
- [Folder Options](./folderoptions.md)
- [Sorting Options](./sortingoptions.md)
- [Filter Settings](./filtersettings.md)
- [Layout Options](./layoutoptions.md)
- [Metadata Options](./metadataoptions.md)
- [Web Part Appearance](./appearance.md)
- [Advanced Options](./advanced.md)
- [Web Part Messages](./message.md)
- [Performance](./performance.md)