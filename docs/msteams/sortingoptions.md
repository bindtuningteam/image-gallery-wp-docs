![sorting-options](../images/classic/24.sortingoptions.png)

Here you can define what will be the order of your items using the internal name of one of your list colunms name.

You will need to type in the internal name of the list column on the Order by text box - we will sort the items according to the value of the cell name you enter.

Here is what you need to do on SharePoint:

1. Access the **Teams Settings**

    ![settings_edit.png](../images/msteams/01.edit.png)

2. Click on the icon to open the **List**.

    ![visit_list](../images/msteams/04.visit_list.png)

4. On the top menu, click on **Library Settings**

    ![visit_list](../images/msteams/05.library_settings.png)

5. On the Columns section, click to open the colum name you want to use

6. Inside, on the URL look for "...Field=...".

7. Copy the internal name

    ![internal-column-name](../images/classic/25.internalcolumnname.png)

6. Now paste the name on the text box;

7. Choose the order, Ascending or Descending.

