1. Open the Team on **Teams panel** that you intend to delete; 
2. Click on the dropdownn.

	![settings_delete.png](../images/msteams/03.delete.png)

3. Click on the **Remove** icon;
4. The Tab will be deleted from you team. 