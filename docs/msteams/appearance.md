![09.wpapperance.png](../images/modern/09.wpapperance.png)

### Right to Left

Using this option will change the web part's text orientation to go from right to left. 
The forms will not be affected.