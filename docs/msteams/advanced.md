![Add section](../images/modern/03.advancedoptions.png)

___
### Title Link 

Note this option is only valid if you have a Title on the Web Part.<br>
On the **Title** of the Web Part, you can add an URL to redirect you to any page. The Web Part will always open the new URL in a new tab of your browser and that option is only visible if you're not in **Edit** mode.

___
### Allow users to subscribe alerts

This feature allows you to create an alert to be notified when a new item is added to the list. A Notification icon will be visible on the web part which will trigger the SharePoint alert option for the list. You need to fill the form to be notified or see the alerts created on the site.  

<p class="alert alert-info">This option is only available when you're not using a target folder as a provider of the image to display on the web part.</p>

___
### Allow users to upload images
 
If you activate this option, users with permissions to manage the connected document library, can upload images by dragging and dropping it to the WebPart zone.

<p class="alert alert-warning"><strong>Warning:</strong> There is a 2MB size limit per image. Uploads only are available if the options Target Folder on Folder Options and/or Folder Structure on Layout Options are activated.</p>
 
___
### Show upload button

If you activate this option, an upload button will be available for the users that can upload images.