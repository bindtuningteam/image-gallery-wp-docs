![folder-options](../images/classic/37.folderoptions.png)

Here you can define a target subfolder of the documents library.

<p class="alert alert-info">If you specify a target folder, the WebPart only shows the images of that specific folder and it's not possible to navigate through other folders</p>