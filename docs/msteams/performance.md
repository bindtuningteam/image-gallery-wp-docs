![performance](../images/modern/04.performance.png)

This feature allows the web part to store content so it doesn't have to connect to the lists every time the page loads.
For more information about this feature, please visit the <a href="https://support.bindtuning.com/hc/en-us/articles/360012991612" target="_blank">next link</a>.

___
### No Caching
By checking this option, the web part will update at every page load.

___
### Page Caching

With **Page Caching** you can store the web part HTML directly onto the page. Updates only when web part is updated and page is checked out (if necessary). This means when the custom forms are submited. User targeting does not work as a result of this.

___
### Local Storage Caching 

Stores the web part HTML in the browser's local storage and updates every 30 minutes. You can choose between **Automatically**, **Manually** and **On page Load**,. 
	
- **Automatically** - With this options, once the local storage gets updated, web parts on the page with stale content will reload to display the fresh content automatically. 

- **Manually (Button)** - With this option, a prompt will appear on the page asking whether you'd like to refresh the content on the page. If so, all web parts on the page with stale content will be reloaded to display the fresh content.

- **On page Load** - By default nothing will happen and the stale content will continue to be presented on the page until the next page visit. 