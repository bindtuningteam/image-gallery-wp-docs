You can choose which format of the file that the web part display. You can choose between the following formats: 

<ul style="column-count: 2">
    <li>PNG</li>
    <li>JPG</li>
    <li>JPEG</li>
    <li>BMP</li>
    <li>GIF</li>
    <li>MP4</li>
    <li>SVG</li>
    <li>ZIP</li>
    <li>PDF</li>
    <li>AI</li>
    <li>PSD</li>
</ul>