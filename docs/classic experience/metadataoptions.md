![generaloptions](../images/classic/40.metadataoptions.png)

___
### Metadata Tags

Image Gallery WebPart shows Metadata that may exist on the image. With this option, is possible to choose a set of metadata tags that will be searched on the image. The options are:

- **All Tags** - All available tags
- **Default** - A pre selected set of tags
- **Custom** - Choose wich tags you want to display
- **None** - Disables all metadata information

___
Example of metadata information with GPS:

![metadata](../images/classic/39.metadataexample.png)