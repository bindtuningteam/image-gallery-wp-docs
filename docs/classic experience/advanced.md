![insert-tab](../images/classic/32.advancedoptions.png)

___
### Allow users to upload images
 
 If you activate this option, users with permissions to manage the connected document library, can upload images by dragging and dropping it to the WebPart zone.

 <p class="alert alert-warning"><strong>Warning:</strong> There is a 2MB size limit per image. Uploads only are available if the options Target Folder on Folder Options and/or Folder Structure on Layout Options are activated.</p>
 
___
### Show upload button
 
If you activate this option, an upload button will be available for the users that can upload images.

___
### Allow users to subscribe alerts

This feature allows you to create an alert to be notified when a new item is added to the list. A Notification icon will be visible on the web part which will trigger the SharePoint alert option for the list. You need to fill the form to be notified or see the alerts created on the site.  

<p class="alert alert-info">This option is only available when you're not using a target folder as a provider of the image to display on the web part.</p>

___
### Language

From here you can select which language you want to be displayed by the web part. This will also translate the forms, but a refresh may be required before you see the changes.

By default, the BindTuning Web Part provides the language for:

- English
- Portuguese

If you'd like to localize the web part to your own language, please refer to [this article](https://support.bindtuning.com/hc/en-us/articles/115004585263).