![generaloptions](../images/classic/26.layoutoptions.png)

___
### Folder Structure

Enable this option if you want to organize your documents into folders

___
### Tiles Spacing

Choose the size of the gap (in pixels) between tiles.

___
### Rows per Page

Use this option to decide how many rows of items you want to see per page. 
You can use this option to decide how much vertical space you want your web part to take.

___
### Force rows

Use this option to ensure the WebPart's height will always have the same size that fits the number of rows per page.
