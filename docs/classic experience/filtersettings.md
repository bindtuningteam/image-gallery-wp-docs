![generaloptions](../images/classic/38.filtersettings.png)
___

### Filters

Here you define which columns of your document library will be used as filtering options. Each selected filter will be displayed as a dropdown element, with filter values.

The following example shows 2 filters: Extension and Author (therefore 2 mapped columns):

|**Configuration**|**Result**|
|-|-|
|![dropdowns_filters.png](../images/classic/30.dropdowns_internal.png)|![dropdowns_filters.png](../images/classic/29.dropdowns_filters.png)|

If no filters are defined, no dropdowns/filters will appear on the web part.<br />
To use a column as a filter, you need to place it's **internal name** into the field.<br />
To show a custom **display name** on the dropdown, place your text inside square brackets<br />
To have a **default value** selected, add two colons after the interal or display name followed by the selected value.

**e.g.: Dprt[Department]::HR, Topic::Web**

Here is what you need to do to check a column's **internal name**: 
1. Access **Site contents** and open your list;
2. On the top menu, click on **List** and then **List Settings** (or **Settings** > **List Settings**);
3. On the **Columns** section, click to open the colum name you want to use;
4. Inside, on the URL look for **"...&Field=..."**.  
5. Copy the internal name;
6. Now paste the name on the text box;

![internalcolumnname](../images/classic/31.internalcolumnname.png) 

____
### Open Filters On

Here you are able to choose how to open the dropdown, on **Click** or on **Hover**.

___
### Collapsible

If enabled, the filter dropdowns will be hidden by default and a button will be added to toggle visibility. If disabled, the filters will always be visible.

___
### Search Bar

If enabled, a search bar is added to the top of the web part for filtering content. 
The search bar will always be enabled if any filters are set.