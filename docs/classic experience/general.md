On the Web Part Properties panel, you've multiple options which you can edit for different configuration of the Web Part.


![generaloptions](../images/classic/01.generaloptions.gif)
 
- [Images Source](./listsettings.md)
- [Folder Options](./folderoptions.md)
- [Sorting Options](./sortingoptions.md)
- [Filter Settings](./filtersettings.md)
- [Layout Options](./layoutoptions.md)
- [Metadata Options](./metadataoptions.md)
- [Advanced Options](./advanced.md)
- [Performance](./performance.md)
- [Web Part Appearance](./appearance.md)
- [Web Part Messages](./message.md)


The global settings form let you apply options to **all the web parts** on the page at once. To use the form, follow the steps:

- [Configure Global Settings](./globalsettings)

![globalsettings](../images/classic/05.globalsettings.png)