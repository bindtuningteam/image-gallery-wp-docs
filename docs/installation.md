If you intend to install the Image Gallery BindTuning Web Part for <b>SharePoint</b> please go to the <a href="https://bindtuning-setup-web-part.readthedocs.io/en/latest/" target="_blank">Setup Guide</a>.


If you intend to install the Image Gallery BindTuning Web Part for <b>Microsoft Teams</b>, please go to the <a href="https://bindtuning-setup-web-part.readthedocs.io/en/latest/ms%20teams/installation/" target="_blank">Setup Guide</a>.

If you intend to install the BindTuning My Work Web Part, please visit the <a href="https://bindtuning-setup-web-part.readthedocs.io/en/latest/modern%20experience/installation/" target="_blank">Setup Guide</a>.</p> 

_____
### Tenant API Permissions 

<p class="alert alert-warning">After installing the Web Part, some permissions must be approved on the SharePoint Admin Portal. You just need to approve this Graph Request once.</p>

Steps to approve the permissions:

1. Open your **SharePoint Admin Portal**
2. Click on the **Try Now** button to open the new SharePoint Admin Center 
3. Click on **API Management**
4. Approve the following requests:

    - Files.Read.All
    - Sites.Read.All

5. The Web Part is ready to be added to a Modern Page. 

_____
### Exchange

If you have a hybrid environment, please check if you met the requirements that are needed to retrieve the user information.

- <a href="https://docs.microsoft.com/en-us/graph/hybrid-rest-support#requirements-for-the-rest-api-to-work-in-hybrid-deployments" target="_blank">Hybrid Environment</a>